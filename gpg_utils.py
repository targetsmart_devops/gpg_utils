"""Routines that can be used to automate GPG encryption/decryption file
operations. Originally used in Jobber to support ExactTrack vendor workflows.
The routines use `subprocess``, expecting a working GPG installation.
"""


import sys
import logging
import subprocess

if sys.platform == "win32":
    GPG_CMD = r"C:\Program Files (x86)\GnuPG\bin\gpg.exe"  # https://www.gpg4win.org/
else:
    GPG_CMD = "gpg"


def encrypt_file(infile, outfile, recipient, gpg_cmd=None):
    r"""Encrypt `infile` using the public key associated with `recipient`.

    Before using, you need to import the public key and set full owner trust to
    avoid prompts:

    ```
    gpg --batch --import InfutorDataSolutionsPublicKey_2021.asc

    # Set owner trust using {fingerprint}:{5} where 5 indicates full trust:
    echo '4FC2280A7000B816540422020AD0B88FFA733BE2:5:'|gpg --import-ownertrust --
    ```

    Usage example:

    ```
    encrypt_file('input.csv', 'encrypted.csv.gpg', 'Infutor Data Solutions')
    ```

    Optionally override the location of the `gpg` cli. Defaults:

    - Non-Windows: `gpg`
    - Windows: `C:\Program Files (x86)\GnuPG\bin\gpg.exe`
    """
    if not gpg_cmd:
        gpg_cmd = GPG_CMD

    args = [
        GPG_CMD,
        "--yes",
        "--skip-verify",
        "--quiet",
        "--always-trust",
        "--encrypt",
        "--output",
        str(outfile),
    ]

    if not recipient:
        raise ValueError("Cannot encrypt file.  No recipient was provided.")

    args.append("--recipient")
    args.append(recipient)
    args.append(str(infile))

    logging.info(
        "Encrypting file %s using %s. Output target: %s. Recipient: %s",
        infile,
        gpg_cmd,
        outfile,
        recipient,
    )

    with subprocess.Popen(
        args,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=sys.platform == "win32",
    ) as proc:
        stdout, stderr = proc.communicate()
        retcode = proc.returncode
        if retcode != 0:
            raise Exception(
                f"Encryption failed with nonzero status {retcode}. Stdout: {stdout}. Stderr: {stderr}."
            )
    return True


def decrypt_file(infile, outfile, password, pinentry_mode_supported=True, gpg_cmd=None):
    r"""Decrypt the file encrypted with our private key named `infile` to produce
    the decrypted file `outfile`.

    Only newer versions of GPG cli support the `--pinentry-mode` argument. If
    you get an error that this isn't supported, try with
    `pinentry_mode_supported=False`.

    Optionally override the location of the `gpg` cli. Defaults:

    - Non-Windows: `gpg`
    - Windows: `C:\Program Files (x86)\GnuPG\bin\gpg.exe`

    Before using, you need to import the private key needed to decrypt the file:

    ```
    gpg --batch --import ./pki/tsmart_private.asc
    ```

    Usage example:

    ```
    decrypt_file('encrypted.csv.gpg', 'decrypted.csv', '********')
    ```

    """
    if not gpg_cmd:
        gpg_cmd = GPG_CMD

    if not password:
        raise ValueError(
            "Cannot decrypt file without specifying the password associated with the private key."
        )

    args = [
        gpg_cmd,
        "--passphrase-fd",
        "0",
        "--yes",
        "--batch",
        "--skip-verify",
        "--quiet",
        "--always-trust",
        "--decrypt",
        "--output",
        outfile,
        infile,
    ]
    if pinentry_mode_supported:
        args.insert(1, "--pinentry-mode")
        args.insert(2, "loopback")

    logging.info(
        "Decrypting file %s using %s. Output target: %s", infile, gpg_cmd, outfile
    )
    with subprocess.Popen(
        args,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=sys.platform == "win32",
    ) as proc:
        proc.stdin.write(password.encode("utf8") + b"\n")
        stdout, stderr = proc.communicate()
        retcode = proc.returncode
        logging.info("Decryption Return Code %d", retcode)
        logging.info("Decryption stdout: %s", stdout.decode("utf8", "ignore"))
        logging.info("Decryption stderr: %s", stderr.decode("utf8", "ignore"))

        if retcode != 0:
            raise Exception(
                f"Decryption subprocess failed with nonzero status. Stdout: {stdout}. Stderr: {stderr}."
            )

    return True
