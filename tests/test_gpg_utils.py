import os.path
from gpg_utils import encrypt_file, decrypt_file

SECRET = "secret"
RECIPIENT = "Insecure Test Key"

HEREDIR = os.path.abspath(os.path.dirname(__file__))


def test_encrypt_decrypt():
    encrypted_file = os.path.join(HEREDIR, "__testing_output.gpg")
    decrypted_file = os.path.join(HEREDIR, "__test_output.decrypted")
    if os.path.exists(encrypted_file):
        os.unlink(encrypted_file)

    if os.path.exists(decrypted_file):
        os.unlink(decrypted_file)

    encrypt_file(os.path.join(HEREDIR, __file__), encrypted_file, RECIPIENT)
    assert os.path.exists(encrypted_file)
    decrypt_file(encrypted_file, decrypted_file, SECRET, pinentry_mode_supported=False)
    assert os.path.exists(decrypted_file)
    with open(os.path.join(HEREDIR, __file__), "r", encoding="utf8") as orig:
        with open(decrypted_file, "r", encoding="utf8") as decrypted:
            assert orig.read() == decrypted.read()
