from setuptools import setup

setup(
    name="gpg_utils",
    version="0.1",
    description="Routines that can be used to automate GPG encryption/decryption file operations. Originally used in Jobber to support ExactTrack vendor workflows. The routines use `subprocess`, expecting a working GPG installation.",
    author="TargetSmart",
    author_email="devops@targetsmart.com",
    license="Copyright TargetSmart. All rights reserved.",
    url="https://bitbucket.org/targetsmart_devops/gpg_utils.git",
    download_url="https://bitbucket.org/targetsmart_devops/gpg_utils.git",
    test_suite="tests",
    py_modules=["gpg_utils"],
    install_requires=[],
)
