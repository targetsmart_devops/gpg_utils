# gpg_utils


Python helper routines that can be used to automate GPG encryption/decryption
file operations. Originally used in Jobber to support ExactTrack vendor
workflows.


## Installation

``` sh
pip install git+https://git@bitbucket.org/targetsmart_devops/gpg_utils
```


## Usage

### Encrypting a file for a recipient

``` python
from gpg_utils import encrypt_file 

encrypt_file('input.csv', 'encrypted.csv.gpg', 'Infutor Data Solutions')
```

Before using, you need to import the public key and set full owner trust to
avoid prompts:

```bash
gpg --batch --import InfutorDataSolutionsPublicKey_2021.asc

# Set owner trust using {fingerprint}:{5} where 5 indicates full trust:
echo '4FC2280A7000B816540422020AD0B88FFA733BE2:5:'|gpg --import-ownertrust --
```


### Decrypt a file that has been encypted with our private key 

``` python
from gpg_utils import decrypt_file
decrypt_file('encrypted.csv.gpg', 'decrypted.csv', '********')
```

Before using, you need to import the private key needed to decrypt the file:

```bash
gpg --batch --import tsmart_private.asc
```

